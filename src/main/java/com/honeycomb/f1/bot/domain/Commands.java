package com.honeycomb.f1.bot.domain;

public enum Commands {
	CREATE_CHAMPIONSHIP("!createChampionship"),
	END_CHAMPIONSHIP("!endChampionship"),
	CREATE_RACE("!createRace"),
	END_RACE("!endRace")
	;

	public String name;

	Commands(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Commands fromValue(String value) {
		for(Commands command : values()) {
			if(command.getName().equalsIgnoreCase(value)) {
				return command;
			}
		}
		
		return null;
	}
	
}
