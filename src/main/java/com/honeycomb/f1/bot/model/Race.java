package com.honeycomb.f1.bot.model;

import java.time.LocalDateTime;

public class Race extends BaseModel{

	private String name, messageId;
	private Championship championship;
	private int laps;
	private LocalDateTime qualifyDateTime, raceDateTime;
	private Circuit circuit;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMessageId() {
		return messageId;
	}
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	public Championship getChampionship() {
		return championship;
	}
	public void setChampionship(Championship championship) {
		this.championship = championship;
	}
	public int getLaps() {
		return laps;
	}
	public void setLaps(int laps) {
		this.laps = laps;
	}
	public LocalDateTime getQualifyDateTime() {
		return qualifyDateTime;
	}
	public void setQualifyDateTime(LocalDateTime qualifyDateTime) {
		this.qualifyDateTime = qualifyDateTime;
	}
	public LocalDateTime getRaceDateTime() {
		return raceDateTime;
	}
	public void setRaceDateTime(LocalDateTime raceDateTime) {
		this.raceDateTime = raceDateTime;
	}
	public Circuit getCircuit() {
		return circuit;
	}
	public void setCircuit(Circuit circuit) {
		this.circuit = circuit;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((championship == null) ? 0 : championship.hashCode());
		result = prime * result + ((circuit == null) ? 0 : circuit.hashCode());
		result = prime * result + laps;
		result = prime * result + ((messageId == null) ? 0 : messageId.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((qualifyDateTime == null) ? 0 : qualifyDateTime.hashCode());
		result = prime * result + ((raceDateTime == null) ? 0 : raceDateTime.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Race other = (Race) obj;
		if (championship == null) {
			if (other.championship != null)
				return false;
		} else if (!championship.equals(other.championship))
			return false;
		if (circuit == null) {
			if (other.circuit != null)
				return false;
		} else if (!circuit.equals(other.circuit))
			return false;
		if (laps != other.laps)
			return false;
		if (messageId == null) {
			if (other.messageId != null)
				return false;
		} else if (!messageId.equals(other.messageId))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (qualifyDateTime == null) {
			if (other.qualifyDateTime != null)
				return false;
		} else if (!qualifyDateTime.equals(other.qualifyDateTime))
			return false;
		if (raceDateTime == null) {
			if (other.raceDateTime != null)
				return false;
		} else if (!raceDateTime.equals(other.raceDateTime))
			return false;
		return true;
	}
}
