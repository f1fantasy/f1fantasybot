package com.honeycomb.f1.bot.repository;

import static com.mongodb.client.model.Aggregates.match;
import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.honeycomb.f1.bot.model.Bet;
import com.honeycomb.f1.bot.model.Driver;
import com.honeycomb.f1.bot.model.Race;
import com.mongodb.client.model.FindOneAndReplaceOptions;
import com.mongodb.client.model.ReturnDocument;

import net.dv8tion.jda.api.entities.Emote;
import net.dv8tion.jda.api.entities.User;

public class BetRepository extends AbstractRepository<Bet>{
	
	Logger LOG = Logger.getLogger(this.getClass().getName());
	private DriverRepository driverRepository;
	private RaceRepository raceRepository;

	public BetRepository() {
		super(Bet.class);
		this.driverRepository = new DriverRepository();
		this.raceRepository = new RaceRepository();
	}
	
	public void addBet(User user, Emote driverEmote, String raceMessageId) {
		
		Bson raceMatch = match(eq("messageId", raceMessageId));
		Race race = raceRepository.find(raceMatch);
		
		if(race == null) {
			throw new IllegalArgumentException("Could not find race with id " + raceMessageId);
		}
		
		Bson betMatch = match(and(eq("championshipId", race.getChampionship().getId().toString()), 
								  eq("raceId", race.getId().toString()),
								  eq("userId", user.getId())));
		
		Bet userBet = find(betMatch);
		
		if(userBet != null) {
			Driver driver = driverRepository.find(driverEmote.getName());
			LOG.info("Adding driver " + driver.getName() + " to user's " + user.getName() + " bet");
			
			userBet.getDriverList().add(driver);
			userBet.setUserId(user.getId());
			
			Document filterById = new Document("_id", userBet.getId());
			FindOneAndReplaceOptions options = new FindOneAndReplaceOptions().returnDocument(ReturnDocument.AFTER);
			super.findOneAndReplace(filterById, options, userBet);
			
		} else {
			Driver driver = driverRepository.find(driverEmote.getName());
			LOG.info("Adding Bet for user " + user.getName() + " with Driver " + driver.getName());
			
			Bet bet = new Bet();
			bet.setChampionshipId(race.getChampionship().getId().toString());
			bet.setRaceId(race.getId().toString());
			bet.getDriverList().add(driver);
			bet.setUserId(user.getId());
			
			save(bet);
		}
	}

	public Bet find(Bson match) {
		return find(Arrays.asList(match));
	}
	
	public List<Bet> findAll(Bson match) {
		return findAll(Arrays.asList(match));
	}
}
