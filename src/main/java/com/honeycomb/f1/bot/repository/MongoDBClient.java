package com.honeycomb.f1.bot.repository;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.conversions.Bson;

import com.honeycomb.f1.bot.domain.Domains;
import com.mongodb.BasicDBObject;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoException;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.FindOneAndReplaceOptions;

public class MongoDBClient {
	
	private MongoDatabase mongoDatabase;
	private Properties properties;
	ConnectionString connectionString;
	MongoClientSettings clientSettings;
	
	private static MongoDBClient INSTANCE;
	
	public synchronized static MongoDBClient getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new MongoDBClient();
        }
        
        return INSTANCE;
    }
    
    private MongoDBClient() {
    	
    	try (InputStream input = new FileInputStream(Domains.DATABASE_PROPERTY_FILE.getName())) {
    		properties = new Properties();
    		properties.load(input);
    		
    		connectionString = new ConnectionString(properties.getProperty("database.url"));
    		
    		CodecRegistry pojoCodecRegistry = fromProviders(PojoCodecProvider.builder().automatic(true).build());
            CodecRegistry codecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(), pojoCodecRegistry);
            clientSettings = MongoClientSettings.builder()
            									.applyConnectionString(connectionString)
            									.codecRegistry(codecRegistry)
            									.build();
    		
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
	
	public Document findFirst(String collection, BasicDBObject query) {
		try (MongoClient mongoClient = MongoClients.create(clientSettings)) {
			mongoDatabase = getDatabase(mongoClient);
			return mongoDatabase.getCollection(collection).find(query).first();
			
		} catch (MongoException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public ArrayList<Document> findAll(String collection, BasicDBObject query) {
		try (MongoClient mongoClient = MongoClients.create(clientSettings)) {
			mongoDatabase = getDatabase(mongoClient);
			return mongoDatabase.getCollection(collection).find(query).into(new ArrayList<Document>());
			
		} catch (MongoException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public ArrayList<?> findAll(String collection, List<? extends Bson> aggregateList) {
		try (MongoClient mongoClient = MongoClients.create(clientSettings)) {
			mongoDatabase = getDatabase(mongoClient);
			MongoCollection<?> col = mongoDatabase.getCollection(collection);
			return col.aggregate(aggregateList).into(new ArrayList<>());
			
		} catch (MongoException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public Object find(Class<?> collection, List<? extends Bson> aggregateList) {
		try (MongoClient mongoClient = MongoClients.create(clientSettings)) {
			mongoDatabase = getDatabase(mongoClient);
			MongoCollection<?> col = mongoDatabase.getCollection(collection.getSimpleName(), collection);
			return col.aggregate(aggregateList).first();
			
		} catch (MongoException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	public Document save(String collection, Document document) {
		try (MongoClient mongoClient = MongoClients.create(clientSettings)) {
			mongoDatabase = getDatabase(mongoClient);
			mongoDatabase.getCollection(collection).insertOne(document);
			
			return document;
			
		} catch (MongoException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public <T> T save(String collection, T model) {
		try (MongoClient mongoClient = MongoClients.create(clientSettings)) {
			mongoDatabase = getDatabase(mongoClient);
			MongoCollection<T> mongoCollection = (MongoCollection<T>) mongoDatabase.getCollection(collection, model.getClass());
			mongoCollection.insertOne(model);
			
			return model;
			
		} catch (MongoException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public Document findAndUpdate(String collection, BasicDBObject query, BasicDBObject document) {
		try (MongoClient mongoClient = MongoClients.create(clientSettings)) {
			mongoDatabase = getDatabase(mongoClient);
			return mongoDatabase.getCollection(collection).findOneAndUpdate(query, document);
			
		} catch (MongoException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public <T> T findAndUpdate(Document filterById, FindOneAndReplaceOptions options, T model) {
		try (MongoClient mongoClient = MongoClients.create(clientSettings)) {
			mongoDatabase = getDatabase(mongoClient);
			MongoCollection<T> mongoCollection = (MongoCollection<T>) mongoDatabase.getCollection(model.getClass().getSimpleName(), model.getClass());
			return mongoCollection.findOneAndReplace(filterById, model, options);
			
		} catch (MongoException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	private MongoDatabase getDatabase(MongoClient mongoClient) {
		return mongoClient.getDatabase(properties.getProperty("database.name"));
	}
}
