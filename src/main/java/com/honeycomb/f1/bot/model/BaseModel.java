package com.honeycomb.f1.bot.model;

import org.bson.types.ObjectId;

public class BaseModel {
	
	private ObjectId id;

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	} 
}
