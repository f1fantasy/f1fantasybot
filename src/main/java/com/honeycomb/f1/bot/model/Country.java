package com.honeycomb.f1.bot.model;

public class Country extends BaseModel{

	private String name, flagURL;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFlagURL() {
		return flagURL;
	}

	public void setFlagURL(String flagURL) {
		this.flagURL = flagURL;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((flagURL == null) ? 0 : flagURL.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Country other = (Country) obj;
		if (flagURL == null) {
			if (other.flagURL != null)
				return false;
		} else if (!flagURL.equals(other.flagURL))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
