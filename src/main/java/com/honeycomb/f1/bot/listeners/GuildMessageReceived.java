package com.honeycomb.f1.bot.listeners;

import com.honeycomb.f1.bot.commands.CreateChampionshipCommand;
import com.honeycomb.f1.bot.commands.CreateRaceCommand;
import com.honeycomb.f1.bot.commands.EndChampionshipCommand;
import com.honeycomb.f1.bot.commands.EndRaceCommand;
import com.honeycomb.f1.bot.domain.Commands;

import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class GuildMessageReceived extends ListenerAdapter{
	
	private static final String SPACE_SPLIT_PATTERN = "\\s+";
	

	public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
		String[] args = event.getMessage()
							 .getContentRaw()
							 .split(SPACE_SPLIT_PATTERN);
		
		if(isF1FantasyCommand(args)) {
			switch (Commands.CREATE_CHAMPIONSHIP.fromValue(args[0])) {
				case CREATE_CHAMPIONSHIP: {
					new CreateChampionshipCommand(event.getMessage(), event.getChannel()).execute();
					break;
				
				} case END_CHAMPIONSHIP: {
					new EndChampionshipCommand(event.getMessage(), event.getChannel()).execute();
					break;
					
				} case CREATE_RACE: {
					new CreateRaceCommand(event.getMessage(), event.getChannel(), event.getJDA()).execute();
					break;
					
				} case END_RACE: {
					new EndRaceCommand(event.getMessage(), event.getChannel(), event.getJDA()).execute();
					break;
				}
			}
		}
	}

	private boolean isF1FantasyCommand(String[] args) {
		return args[0] != null && args[0].startsWith("!");
	}

}
