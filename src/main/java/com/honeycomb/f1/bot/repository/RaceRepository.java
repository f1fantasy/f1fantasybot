package com.honeycomb.f1.bot.repository;

import java.util.Arrays;
import java.util.logging.Logger;

import org.bson.conversions.Bson;

import com.honeycomb.f1.bot.model.Race;

public class RaceRepository extends AbstractRepository<Race>{
	
	Logger LOG = Logger.getLogger(this.getClass().getName());
	private ChampionshipRepository championshipRepository;

	public RaceRepository() {
		super(Race.class);
		this.championshipRepository = new ChampionshipRepository();
	}
	
	public Race find(Bson match) {
		return find(Arrays.asList(match));
	}
	
	public Race createRace(Race race) {
		return save(race);
	}

}
