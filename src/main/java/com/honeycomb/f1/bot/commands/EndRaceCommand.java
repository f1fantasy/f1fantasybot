package com.honeycomb.f1.bot.commands;

import java.awt.Color;
import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.LinkedList;

import com.honeycomb.f1.bot.data.RaceDataSingleton;
import com.honeycomb.f1.bot.domain.Domains;
import com.honeycomb.f1.bot.domain.Emotes;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Emote;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.TextChannel;

public class EndRaceCommand implements Command {

	private Message message;
	private TextChannel textChannel;
	private JDA discordService;
	
	public EndRaceCommand(Message message, TextChannel textChannel, JDA jda) {
		this.message = message;
		this.textChannel = textChannel;
		this.discordService = jda;
	}
	
	@Override
	public void execute() {
		String[] args = message.getContentRaw().split(Domains.COMMAND_SPLIT_PATTERN.getName());
		
		if(!hasEnoughCommandParameters(args)) {
			textChannel.sendMessage("O comando !createRace requer o nome do circuito como parâmetro. Ex: !createRace Bahrein").queue();
		}
		
		
	}
	

	private boolean hasEnoughCommandParameters(String[] args) {
		return args.length > 1;
	}

	@Override
	public boolean validate() {
		String[] args = message.getContentRaw().split(Domains.COMMAND_SPLIT_PATTERN.getName());
		return args.length > 1;
	}

	
}
