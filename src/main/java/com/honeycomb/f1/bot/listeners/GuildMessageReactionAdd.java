package com.honeycomb.f1.bot.listeners;

import java.util.logging.Logger;

import com.honeycomb.f1.bot.data.RaceDataSingleton;
import com.honeycomb.f1.bot.domain.Emotes;
import com.honeycomb.f1.bot.repository.BetRepository;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Emote;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.MessageEmbed.Field;
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionAddEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.requests.RestAction;

public class GuildMessageReactionAdd extends ListenerAdapter{
	
	private static final String EMOTE_SUFFIX = "> ";
	private static final String EMOTE_SEPARATOR = ":";
	private static final String EMOTE_PREFIX = " <:";
	private static final String SPACE_SPLIT_PATTERN = "**";
	
	Logger LOG = Logger.getLogger(this.getClass().getName());
	BetRepository betRepository = new BetRepository();

	public void onGuildMessageReactionAdd(GuildMessageReactionAddEvent event) {
		
		if(isNotABotReaction(event) && isF1Emote(event.getReactionEmote().getEmote())) {
			
			String raceMessageId = RaceDataSingleton.getInstance().getActiveRaceMessageId();
			LOG.info("Fetching Discord Race Message ID: " + raceMessageId);
			
			RestAction<Message> raceMessage = event.getChannel().retrieveMessageById(raceMessageId);
			
			raceMessage.queue(message -> {
				MessageEmbed embed = message.getEmbeds().stream().findFirst().get();
				EmbedBuilder builder = new EmbedBuilder(embed);
				
				//look for field with user name
				if(!areThereFieldsAnyUserFields(event, embed)) {
					
					builder = createUserBet(event, message, embed);
					
				} else {
					Field betField = embed.getFields().stream()
							  .filter(field -> field.getName().equalsIgnoreCase(SPACE_SPLIT_PATTERN + event.getUser().getName() + SPACE_SPLIT_PATTERN))
							  .findFirst()
							  .get();
					
					builder = editUserBet(event, message, embed, betField);
				}
				
				betRepository.addBet(event.getUser(), event.getReactionEmote().getEmote(), raceMessageId);
				message.editMessage(builder.build()).clearFiles().queue();
			});
		}
	}

	private boolean areThereFieldsAnyUserFields(GuildMessageReactionAddEvent event, MessageEmbed embed) {
		return embed.getFields().stream()
				  			.anyMatch(field -> field.getName().equalsIgnoreCase(SPACE_SPLIT_PATTERN + event.getUser().getName() 
				  					+ SPACE_SPLIT_PATTERN));
	}

	private boolean isF1Emote(Emote emote) {
		return Emotes.HAMILTON.isF1Emote(emote);
	}

	private boolean isNotABotReaction(GuildMessageReactionAddEvent event) {
		return !event.getUser().isBot();
	}

	private EmbedBuilder createUserBet(GuildMessageReactionAddEvent event, Message message, MessageEmbed embed) {
		EmbedBuilder builder = new EmbedBuilder(embed);
		StringBuilder sb = new StringBuilder();
		sb.append(EMOTE_PREFIX)
		  .append(event.getReactionEmote().getEmote().getName())
		  .append(EMOTE_SEPARATOR)
		  .append(event.getReactionEmote().getEmote().getId())
		  .append(EMOTE_SUFFIX);
		
		String fieldHeader = "**" + event.getUser().getName() + "** ";
		Field newField = new Field(fieldHeader,  sb.toString(), false);
		builder.addField(newField);
		return builder;
	}
	
	private EmbedBuilder editUserBet(GuildMessageReactionAddEvent event, Message message, MessageEmbed embed, Field betField) {
		EmbedBuilder builder = new EmbedBuilder(embed);
		StringBuilder sb = new StringBuilder();
		
		sb.append(betField.getValue())
		  .append(" ")
		  .append(EMOTE_PREFIX) 
		  .append(event.getReactionEmote().getEmote().getName())
		  .append(EMOTE_SEPARATOR)
		  .append(event.getReactionEmote().getEmote().getId())
		  .append(EMOTE_SUFFIX);
		
		String fieldHeader = "**" + event.getUser().getName() + "**";
		Field oldUserField = findFieldByHeader(embed, fieldHeader);
		
		if(oldUserField != null) {
			int fieldIndex = builder.getFields().indexOf(oldUserField);
			builder.getFields().set(fieldIndex, new Field(fieldHeader,  sb.toString(), false));
		}
		
		return builder;
	}

	private Field findFieldByHeader(MessageEmbed embed, String userName) {
		return embed.getFields().stream()
								.filter(field -> field.getName().equalsIgnoreCase(userName))
								.findFirst()
								.get();
	}
}
